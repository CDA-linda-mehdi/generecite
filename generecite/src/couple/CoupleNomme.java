package couple;

public class CoupleNomme<T> extends Couple<T> {
	
	private String nom;

	public CoupleNomme(T premier, T second, String nom) {
		super(premier, second);
		this.nom = nom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public void affiche() {
		// TODO Auto-generated method stub
		System.out.println(this);
	}

	@Override
	public String toString() {
		return super.toString() + " [nom=" + nom + "]";
	}
}
