package couple;

public class Couple<T> {

	private T x, y ; // les deux �l�ments du couple
    public Couple (T premier, T second)    {
        x = premier ; y = second ;
    }
    public void affiche (){
        System.out.println ("premiere valeur : " + x
        + " - deuxieme valeur : " + y ) ;
    }
	@Override
	public String toString() {
		return "Couple [x=" + x + ", y=" + y + "]";
	}   
}