package couple;

public class Main {

	public static void main(String[] args) {

		Couple couple1 = new Couple(1, "Loulou");
		Couple couple2 = new CoupleNomme('a', 'b', "Lili");
		Couple couple3 = new PointNomme(1, 2, "Toutou");
		
		couple1.affiche();
		couple2.affiche();
		couple3.affiche();
	}
}
