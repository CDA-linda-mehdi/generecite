package couple;

public class PointNomme extends Couple<Integer> {

	private String name;

	public PointNomme(Integer premier, Integer second, String name) {
		super(premier, second);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void affiche() {
		// TODO Auto-generated method stub
		System.out.println(this);;
	}

	@Override
	public String toString() {
		return super.toString()+ " PointNomme [name=" + name + "]";
	}

}
