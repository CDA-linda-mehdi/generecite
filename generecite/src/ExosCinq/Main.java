package ExosCinq;

public class Main {

	public static void main(String[] args) {
		Integer tab [] = new Integer [3];
		tab[0] = 20;
		tab[1] = 50;
		tab[2] = 10;
		
		String [] tabString = new String [3];
		tabString[0] = "toto";
		tabString[1] = "arlette";
		tabString[2] = "zeroo";
		System.out.println(Tableau.plusGrandElementDuTableau(tab));
		System.out.println(Tableau.plusGrandElementDuTableau(tabString));
	}
}
