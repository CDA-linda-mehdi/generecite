package ExosSix;

public class Point implements Comparable<Point>{

	private int x, y ;
	
    Point (int x, int y) {
        this.x = x ; this.y = y ;
    }
    public void affiche(){
        System.out.println ("coordonnees : " + x + " " + y ) ;
    }
    
    public double distance() {
    	double res = Math.sqrt(Math.pow((0 - this.x),  2) + (Math.pow(0 - this.y, 2)));
    	return res;
    }
	
	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
	
	@Override
	public int compareTo(Point o) {
		if (this.distance() > o.distance()) {
			return 1;
		} else  {
			return -1;
		}
	}
}
