package ExosSix;

public class Tableau {

	//M�thode g�n�rique
	//Elle g�re tous les tableaux dont les �l�ments sont d'un type qui impl�mente l'interface comparable
	public static <T extends Comparable<T>> T plusGrandElementDuTableau(T[] tab) {
		
		T save = tab[0];
		for (T t : tab) {
			if ((t.compareTo(save)) > 0) {
				save = t;
			}
		}
		return save;
	}
}
