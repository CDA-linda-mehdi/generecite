package tripletH;

public class Main {

	public static void main(String[] args) {

		TripletH<Character, Integer, Boolean> obj1 = new TripletH<>('l', 7, true);
		
		TripletH<String, String, Integer> obj2 = new TripletH<>("�a", "vaut", 155);
		
		TripletH<Double, Float, Float> obj3 = new TripletH<>(5.7d, 8.2f, 141f);
		
		obj1.affiche();
		obj2.affiche();
		obj3.affiche();
	}

}
