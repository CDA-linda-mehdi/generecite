package tripletH;

public class TripletH <T1, T2, T3>{

	private final T1 premier;
	private final T2 second;
	private final T3 troisieme;
	
	public TripletH(T1 premier, T2 second, T3 troisieme) {
		this.premier = premier;
		this.second = second;
		this.troisieme = troisieme;
	}
	
	public <T1, T2, T3> void affiche() {
		System.out.println(this.premier + " " + this.second + " " + this.troisieme);
	}

	public T1 getPremier() {
		return premier;
	}

	public T2 getSecond() {
		return second;
	}

	public T3 getTroisieme() {
		return troisieme;
	}
	
	
}
