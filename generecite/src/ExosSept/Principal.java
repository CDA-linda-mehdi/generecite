package ExosSept;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean a = true;
		String type;
		String rep;

		System.out.println("Cr�er un ensemble");
		System.out.println("Veuillez choisir le type des �l�ments de l'ensemble : ");
		System.out.println("- Integer");
		System.out.println("- Float");
		System.out.println("- String");
		
		Ensemble<?> liste = new Ensemble<>();
		type = sc.nextLine();
		
		String str = null;
		
		while (a) {
			System.out.println("");
			System.out.println("**********************     MENU     ***************************");
			System.out.println("");
			System.out.println("Veuillez choisir une des options ci-dessous :");
			System.out.println("2 - Ajouter un �l�ment");
			System.out.println("3 - Enlever un �l�ment");
			System.out.println("4 - Afficher la liste");
			System.out.println("5 - Arr�ter le programme");
			rep = sc.nextLine();
			
			switch (rep) {
			case "1" :
				break;
			case "2" :
				System.out.println("Ajouter un �l�ment : ");
				str = sc.nextLine();
				if(type.equals("Integer")) {
					((Ensemble<Integer>) liste).ajouter(Integer.parseInt(str));
				} else if(type.equals("String")) {
					((Ensemble<String>) liste).ajouter(str);
				} else if (type.equals("Float")) {
					((Ensemble<Float>) liste).ajouter(Float.parseFloat(str));
				}
				break;
			case "3" :
				System.out.println("Entrez l'�l�ment � retirer : ");
				str = sc.nextLine();
				if (type.equals("Integer")) {
					((Ensemble<Integer>) liste).retirer(Integer.parseInt(str));
				} else if (type.equals("String")) {
					((Ensemble<String>) liste).retirer(str);
				} else if (type.equals("Float")) {
					((Ensemble<Float>) liste).retirer(Float.parseFloat(str));
				}
				break;
			case "4" :
				System.out.println("4 - Afficher la liste");
				System.out.println(liste);
				break;
			case "5" :
				System.out.println("Arr�t du programme");
				a = false;
				break;
				default :
					System.out.println("Vous n'avez pas saisi une option de la liste !");
			}
		}
		sc.close();
	}
}
