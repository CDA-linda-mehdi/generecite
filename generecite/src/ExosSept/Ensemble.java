package ExosSept;

import java.util.ArrayList;
import java.util.List;

public class Ensemble<T> {

	private final List<T> liste;
	
	public Ensemble() {
		this.liste = new ArrayList<T>();
	}
	
	public boolean ajouter(T element) {
		if (this.liste.contains(element)) {
			return true;
		} else {
			this.liste.add(element);
			return false;
		}
	}
	
	public boolean retirer(T element) {
		return this.liste.remove(element);
		
	}

	@Override
	public String toString() {
		return "Ensemble de " + liste.size() + " �l�ments. \n"
				+ "Les �l�ments sont : " +liste;
	}

	public List<T> getListe() {
		return liste;
	}
	
}
